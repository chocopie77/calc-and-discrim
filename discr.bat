@echo off
title discriminant
echo =============================
echo = discriminant =
echo =============================
echo.
set /p sum=enter a:
set /a a= sum
echo.
set /p sum1=enter b:
set /a b= sum1
echo.
set /p sum3=enter c:
set /a c= sum3
echo.
set /a D=(%b%*%b%)-(4*%a%*%c%)

echo %D%
if %D% LSS 0 ( goto 0)
if %D% == 0 ( goto 1)
if %D% GTR 0 ( goto 2)


:0
echo no roots!
echo.
echo enter "y" to enter calculator
echo.
pause
exit
:1
echo root one
echo.
pause
exit
:2
echo two roots!
echo.
pause
exit