@echo off
title calculator
echo =================
echo = calculator =
echo =================
echo.
set /p sum=Please enter the question:
echo.
set /a ans=%sum%
echo.
if errorlevel 1 ( goto zero)
echo The Answer :%ans%
echo.
:zero
pause
exit